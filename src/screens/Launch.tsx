import React from 'react';
import { TouchableOpacity, StyleSheet, Text, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Strings } from '../constants/Strings';
import {Colors} from '../constants/Colors';

interface Props {
    navigation: any
}

const Launch: React.FC<Props> = (props) => {
    const onPressButton = () => props.navigation.navigate(Strings.searchScreen);

    return (
        <LinearGradient colors={[Colors.gradientStart, Colors.gradientIntermediate, Colors.gradientFinal]} style={styles.root}>
            <Image source={require('../../assets/cocktail.png')} style={styles.image} />
            <Text>
                <Text style={[styles.appName, { fontWeight: 'bold' }]}>{Strings.appNamePart1}</Text>
                <Text style={styles.appName}>{Strings.appNamePart2}</Text>
            </Text>
            <TouchableOpacity
                style={styles.button}
                onPress={onPressButton}
            >
                <Image source={require('../../assets/searchc.png')} style={styles.buttonImage} />
                <Text style={styles.buttonTitle}>{Strings.searchButton}</Text>
            </TouchableOpacity>
        </LinearGradient>
    );
}

export default Launch;

const styles = StyleSheet.create({
    root: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 20,
    },
    button: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        marginTop: 20,
        alignSelf: 'stretch',
        backgroundColor: Colors.white
    },
    appName: {
        color: Colors.white,
        fontSize: 24
    },
    image: {
        width: 50,
        height: 50
    },
    buttonImage: {
        width: 20,
        height: 20
    },
    buttonTitle: {
        textAlign: 'center'
    }
});
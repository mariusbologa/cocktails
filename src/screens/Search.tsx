import React, { useState } from 'react';
import { Text, View, ActivityIndicator, Image, FlatList, StyleSheet } from 'react-native';
import SearchBar from 'react-native-search-bar';
import LinearGradient from 'react-native-linear-gradient';
import { useSearchApi } from '../hooks/FetchData';
import { Strings } from '../constants/Strings';
import { Colors } from '../constants/Colors';
import {RowDrink} from '../store/Models';

const Item: React.FC<RowDrink> = (props) => {
    return (
        <View style={styles.listEntryContainer}>
        <View style={styles.listEntry}>
            <View style={styles.imageContainer}>
                <Image style={styles.drinkThumb} source={{ uri: props.imageUrl}} />
            </View>
            <Text style={styles.title}>{props.name}</Text>
        </View>
    </View>
    );
  }

export default function Search() {
    const [query, setQuery] = useState<string>('');
    const [api] = useSearchApi('', { drinks: [] });
    const search1 = React.createRef<SearchBar>();

    const updateUrl = (text: string) => {
        if (text.length > 2) {
            setQuery(text.trimLeft())
            api.setUrl(`https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${query}`)
        }
    }

    return (
        <View style={[styles.container, styles.root]}>
            <SearchBar
                placeholder={Strings.searchBarPlaceholder}
                ref={search1}
                onChangeText={text =>  updateUrl(text)}
                onCancelButtonPress={() => api.setUrl('') }
            />
            <LinearGradient colors={[Colors.gradientStart, Colors.gradientIntermediate, Colors.gradientFinal]} style={styles.root}>
                {api.state.isLoading ? (<ActivityIndicator size='large' color={Colors.white} />) :
                    (<FlatList
                        data={api.state.data.drinks}
                        keyExtractor={item => item.idDrink}
                        showsVerticalScrollIndicator={false}
                        removeClippedSubviews={true}
                        contentContainerStyle={styles.contentContainer}
                        renderItem={({ item }) => <Item imageUrl={item.strDrinkThumb} name={item.strDrink} />}
                    />
                    )}
            </LinearGradient>
        </View>)
}


const styles = StyleSheet.create({
    root: {
        flex: 1
    },
    imageContainer: {
        height: 80,
        width: 80,
        borderRadius: 40,
        overflow: 'hidden',
    },
    title: {
        marginLeft: 20
    },
    drinkThumb: {
        width: 80,
        height: 80
    },
    container: {
        alignContent: 'center',
        justifyContent: 'center'
    },
    contentContainer: {
        flexGrow: 1
    },
    listEntryContainer: {
        padding: 10 
    },
    listEntry: {
        flex: 1,
        height: 100,
        alignItems: 'center',
        backgroundColor: Colors.white,
        flexDirection: 'row',
        borderRadius: 10,
        padding: 10
    }
})

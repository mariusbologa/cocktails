export const Strings = {
    initialScreen: 'Launch',
    appNamePart1: 'Cocktail',
    appNamePart2: 'Finder',
    searchButton: 'Search your favourite cocktail',
    searchScreen: 'Search',
    searchBarPlaceholder: 'Search'
}
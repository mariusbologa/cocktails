export const Colors = {
    white: 'white',
    black: 'black',
    gradientStart: '#BA2F8A',
    gradientIntermediate: '#C83F59',
    gradientFinal: '#D14B3E'
}
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Strings } from '../constants/Strings';
import Launch from '../screens/Launch';
import Search from '../screens/Search';

const AppNavigator = createStackNavigator({
    Launch: {
        screen: Launch,
        navigationOptions: {
            header: null,
            headerBackTitle: null
        }
    },
    Search: {
        screen: Search,
        navigationOptions: {
            headerTintColor: 'black',
        }
    },
}, {
    initialRouteName: Strings.initialScreen
});

export default createAppContainer(AppNavigator)
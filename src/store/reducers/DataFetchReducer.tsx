import {ActionEnum} from '../../constants/ActionsNames';
import {Drinks} from '../models';

interface Response {
    data: Drinks;
    isLoading: boolean;
}


export type Action =
  | { type: ActionEnum.FETCH_INIT }
  | { type: ActionEnum.FETCH_FAILURE }
  | { type: ActionEnum.FETCH_SUCCESS; payload: Drinks };

export const dataFetchReducer = (state: Response, action: Action) => {
    switch (action.type) {
        case ActionEnum.FETCH_INIT:
            return {
                ...state,
                isLoading: true
            };
        case ActionEnum.FETCH_SUCCESS:
            return {
                ...state,
                isLoading: false,
                data: action.payload,
            };
        case ActionEnum.FETCH_FAILURE:
            return {
                ...state,
                isLoading: false,
                data: {drinks: []}
            };
        default:
            throw new Error();
    }
};
import React, { useState, useEffect, useReducer } from 'react';
import axios, { AxiosResponse, AxiosError } from 'axios';
import  {ActionEnum} from '../constants/ActionsNames';
import {dataFetchReducer} from '../store/reducers/DataFetchReducer';
import {Drinks} from '../store/models';

export const useSearchApi = (initialUrl: string, initialData: Drinks) => {
    const [url, setUrl] = useState<string>(initialUrl);
    const [state, dispatch] = useReducer(dataFetchReducer, {
        isLoading: false,
        data: initialData,
    });

    useEffect(() => {
        let didCancel = false;
        const fetchData = async () => {
            dispatch({ type: ActionEnum.FETCH_INIT});
            axios.get<Drinks>(url).then((response: AxiosResponse<Drinks>) => {
                if (!didCancel) {
                    dispatch({ type: ActionEnum.FETCH_SUCCESS, payload: response.data });
                }
            }).catch((error: AxiosError) => {
                if (!didCancel) {
                    dispatch({ type: ActionEnum.FETCH_FAILURE });
                }
            })
        };
        fetchData();
        return () => {
            didCancel = true;
        };
    }, [url]);
    return [{ state, setUrl }];
};
